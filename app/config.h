#ifndef __CONFIG_H__
#define __CONFIG_H__

/* 根据系统资源及需求确定模块个数 */
#define TIMER_COUNT         (2)   /* 定义定时器个数*/
#define UART_COUNT          (1)   /* 串口个数,51单片机只有一个 */

#endif