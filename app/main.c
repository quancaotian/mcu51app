#include <stdio.h>
#include "config.h"
#include "basetype.h"
#include "platform.h"
#include "trace.h"
#include "board.h"
#include "uart.h"
#include "gpio.h"
#include "timer.h"
#include "systick.h"

#define TEST_UART	    1
#define TEST_LCD1602    0
#define TEST_DIGITRON   0
#define TEST_I2C        0
#define TEST_RTC        0

#if TEST_LCD1602
#include "lcd.h"
#endif

#if TEST_DIGITRON
#include "digitron.h"
#endif

#if TEST_I2C
#include "i2c.h"
#endif

#if TEST_RTC
#include "rtc.h"
#endif


static uint8 digital= 0;

void task_on_timer0(void)
{
    if(gpio_get(LED_L6))
    {
        gpio_reset(LED_L6);
    }
    else
    {
        gpio_set(LED_L6);
    }
}

void task_on_timer1(void)
{
    digital++;
}

/*****************************************************
                Main ������
 *****************************************************/
void main()
{
    tid_t tid0,tid1;
    platform_init();
    board_init();
    systick_init(100);
    uart_init(UART_COM0);
    
    #if 1
    /* ��ʼ����ʱ�� */
    tid0=timer_alloc(TIMERTYPE_PERIODIC,500,task_on_timer0);
    if (tid0<0)
    {
        TRACE_ERR("timer_alloc 0 error!");
    }
    timer_start(tid0);
    
    tid1=timer_alloc(TIMERTYPE_PERIODIC,1000,task_on_timer1);
    if (tid1<0)
    {
        TRACE_ERR("timer alloc 1 error!");
    }
    timer_start(tid1);
    #endif
    
    #if TEST_UART   //----���ڲ�������--------
    {
        char tmpchr;
        
        while(1)
        {
            if (uart_getchar(UART_COM0, &tmpchr))
            {
                //printf("STM32:\r\n");
                uart_putchar(UART_COM0, tmpchr);
            }
        }
    }
    #elif TEST_LCD1602  //----LCD1602��������--------
    {
        lcd_init();
    	lcd_clear();
    	lcd_draw_text(0, 0, "Hello world!    ");
        lcd_draw_text(0, 1, "    Hytera @2014"); 
        while(1)
        {
            ;
        }
    }
    #elif TEST_DIGITRON //----����ܲ�������--------
    {
        while(1)
        {
            digitron_display(0, digital/10);
            digitron_display(1, digital%10);
            delay_ms(20);
        }
    }
    #elif TEST_I2C      //----I2C��������--------
    {   
        uint8 value=0x00;
        i2c_init(0xa0);
        i2c_reg_write(0xa0, 0x02, 0xEE);
        delay_ms(100);
        i2c_reg_read(0xa0, 0x02, &value);
        TRACE_HEX("i2c read:", value, "\r\n");
        while(1)
        {
            delay_ms(20);
        }

    }
    #elif TEST_RTC      //----DS1302ʵʱʱ�Ӳ�������--------
    {
        RTCTIME_S rtc;
        rtc.year = 14;
        rtc.month= 8;
        rtc.date = 8;
        rtc.hour = 0;
        rtc.min  = 0;
        rtc.sec = 0;
        rtc_settime(&rtc);
        
        while(1)
        {
            delay_ms(1000);
            rtc_gettime(&rtc);
            TRACE_REL("TIME: ");
            TRACE_DEC("",20,"");
            TRACE_DEC("",rtc.year,"");
            TRACE_REL("-");
            TRACE_DEC("",rtc.month,"");
            TRACE_REL("-");
            TRACE_DEC("",rtc.date,"");
            TRACE_REL(" ");
            TRACE_DEC("",rtc.hour,"");
            TRACE_REL(":");
            TRACE_DEC("",rtc.min,"");
            TRACE_REL(":");
            TRACE_DEC("",rtc.sec,"");
            TRACE_REL("\r\n");
        }
    }
    #endif
    
    while(1)
    {
        msleep(500);
		gpio_reset(LED_L7);
		msleep(500);
		gpio_set(LED_L7);
    }
}

