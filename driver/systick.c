#include <stdio.h>
#include "systick.h"
#include "trace.h"
#include "board.h"
#include "platform.h"

extern void isr_systick_doticker(void);

uint8 SYSTEM_TICKS_MS=0;
uint8 systick_gettickms(void)
{
    return SYSTEM_TICKS_MS;
}

static uint8 th_reloader=0x00;
static uint8 tl_reloader=0x00;
void systick_init(uint8 ticks_ms)
{
    /* 11.0592M晶振时,12分频后为921600Hz,则10ms为9216个周期
     * 因此初值为65536-9216=56320(0xDC00) */
    uint16 timer_reloader;
    if (ticks_ms<10)
    {
        SYSTEM_TICKS_MS = 10;
    }
    else if (ticks_ms>50)
    {
        SYSTEM_TICKS_MS = 50;
    }
    else
    {
        SYSTEM_TICKS_MS = ticks_ms;
    }
    timer_reloader = (65536-9216*(SYSTEM_TICKS_MS/10));
    th_reloader = timer_reloader/256;
    tl_reloader = timer_reloader%256;
    TMOD |= 0x01;   /* 选择模式1,16位定时器 */
    TH0   = th_reloader;
    TL0   = tl_reloader;
    EA    = 1;      /* 开总中断 */
    ET0   = 1;      /* 定时器中断打开 */
    TR0   = 1;      /* 启动定时器 */
}

/* 中断处理函数 */
void isr_time0_handler(void) interrupt 1 using 1
{
    TH0 = th_reloader;
    TL0 = tl_reloader;
    ET0 = 0;
    isr_systick_doticker();
    ET0 = 1;
}


