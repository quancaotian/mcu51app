#ifndef __I2C_H__
#define __I2C_H__
/**********************************************************
 File   : i2c.h
 Bref   : I2C接口头文件 
 Version: V1.0.0
 Author : FergusZeng
 Date   : 2013-12-08
***********************************************************/

enum
{
    I2CBUS_0=0,
    I2CBUS_1,
    I2CBUS_2,
};

void i2c_select(uint8 i2cbus);
void i2c_init(uint8 i2cbus);
sint32 i2c_reg_read(uint8 i2cdev, uint8 reg, uint8* value);
sint32 i2c_reg_write(uint8 i2cdev, uint8 reg, uint8 value);
sint32 i2c_read(uint8 i2cdev, uint8 * wbuf, sint32 wlen, uint8 * rbuf, sint32 rlen);
sint32 i2c_write(uint8 i2cdev, uint8 * data, int len);

#endif

