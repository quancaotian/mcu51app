#ifndef __DIGITRON_H__
#define __DIGITRON_H__
#include "board.h"

void digitron_display(uint8 pos, uint8 digital);

#endif