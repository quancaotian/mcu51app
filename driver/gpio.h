#ifndef __GPIO_H__
#define __GPIO_H__
/**********************************************************
 File   : gpio.h
 Bref   : GPIO接口头文件 
 Version: V1.0.0
 Author : FergusZeng
 Date   : 2013-12-08
***********************************************************/

#include "platform.h"
/*----------------------------------------------
 *在C51中GPIO定义为sbit类型,不可作为函数参数，
 *所以这里操作GPIO的函数定义为宏,而不是函数。
 ----------------------------------------------*/
#define gpio_init(gpio, mode, speed)	    {;}
#define gpio_get(gpio)                      (gpio)
#define gpio_set(gpio)				        {(gpio)=1;}
#define gpio_reset(gpio)			        {(gpio)=0;}
#define gpio_readbyte(gpio, offset)         (gpio)
#define gpio_writebyte(gpio, offset, byte)  {(gpio)=(byte);}

#endif

