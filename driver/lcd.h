#ifndef __LCD_H__
#define __LCD_H__
#include "basetype.h"

enum
{
    LCD_DRAW_MODE_NORMAL,
    LCD_DRAW_MODE_XOR,
};

void lcd_init(void);
void lcd_clear(void);

void lcd_off(void);
bool lcd_set_bright(uint8 level);
bool lcd_set_backlight(bool on);
bool lcd_set_mode(uint8 mode);

void lcd_draw_chr(uint8 chr, uint8 x, uint8 y);
void lcd_draw_wchr(uint16 wchr, uint8 x, uint8 y);
void lcd_draw_text(uint8 x, uint8 y, const char* text);
void lcd_draw_cursor(uint8 x, uint8 y);

#endif