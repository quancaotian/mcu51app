#include "basetype.h"
#include "lcd.h"
#include "gpio.h"
#include "board.h"
#include "platform.h"
#include "trace.h"
#include <stdio.h>

#define LCD_WIDTH   (16)
#define LCD_HEIGHT  (2)

/* 定义LCD控制线 */
#define LCD_RS          GPIO_DB1
#define LCD_RW          GPIO_DB2
#define LCD_EN          GPIO_LCDEN
#define LCD_DATA        P0
#define LCD_BUSY_BIT    (P0 & 0x80)

static void lcd_write_data(uint8 wdata)
{
    LCD_DATA=wdata;    
}


static bool lcd_is_busy(void) 
{ 
    bool ret;
    gpio_reset(LCD_RS);
    gpio_set(LCD_RW);
    usleep(2);
    gpio_set(LCD_EN);
    ret = !!(LCD_BUSY_BIT);
    gpio_reset(LCD_EN);
    return ret;

}
static void lcd_send_cmd(uint8 cmd)
{
    while(lcd_is_busy());
	gpio_reset(LCD_RS);
	gpio_reset(LCD_RW);
	gpio_reset(LCD_EN);
	usleep(2);
	lcd_write_data(cmd);
	usleep(4);
	gpio_set(LCD_EN);
	usleep(4);
	gpio_reset(LCD_EN);	
}

static void lcd_send_data(uint8 wdata)
{
    while(lcd_is_busy());
	gpio_set(LCD_RS);
	gpio_reset(LCD_RW);
	gpio_reset(LCD_EN);
	lcd_write_data(wdata);
	usleep(4);
	gpio_set(LCD_EN);
	usleep(4);
	gpio_reset(LCD_EN);	
}

void lcd_init(void)
{
    gpio_reset(LCD_RW);     /* 只写数据 */
    msleep(15);   
    lcd_send_cmd(0x01);     /* 清除LCD的显示内容 */         
    lcd_send_cmd(0x38);     /* 16*2显示，5*7点阵，8位数据 */
    msleep(5);
    lcd_send_cmd(0x38);         
    msleep(5);
    lcd_send_cmd(0x38);         
    msleep(5);

    lcd_send_cmd(0x0c);      /* 开显示，不显示光标 */ 
    msleep(5);

    lcd_send_cmd(0x01);      /* 清除LCD的显示内容 */
    msleep(5);
}

void lcd_clear(void)
{
    lcd_send_cmd(0x01); 
    msleep(5);
}

void lcd_draw_chr(uint8 chr, uint8 x, uint8 y)
{
    if ((x>=LCD_WIDTH) ||
        (y>=LCD_HEIGHT))
    {
        return;
    }
    
    if(0==y)
    {
        lcd_send_cmd(0x80+x);
    }
    else
    {
        lcd_send_cmd(0x80+0x40+x);
    }
    lcd_send_data(chr);
}
void lcd_draw_wchr(uint16 wchr, uint8 x, uint8 y)
{
    uint8 chr;
    if ((x>=LCD_WIDTH) ||
        (y>=LCD_HEIGHT))
    {
        return;
    }
    chr = (wchr>>8)&0xFF;
    lcd_draw_chr(chr, x, y);
    chr = wchr & 0xFF;
    lcd_draw_chr(chr, x+1, y);
}
void lcd_draw_text(uint8 x, uint8 y, const char* text)
{
    uint8 i=0;
    uint16 wchr;
    if ((x>=LCD_WIDTH) ||
        (y>=LCD_HEIGHT))
    {
        return;
    }
    while(text[i]) 
 	{
 	    if(text[i]>0x80 && x<LCD_WIDTH)
 	    {
            wchr = text[i];
            wchr = (wchr<<8) | text[i+1];
            lcd_draw_wchr(wchr, x, y);
            i+=2;
            x+=2;
 	    }
 	    else
 	    {
            lcd_draw_chr(text[i++],x++,y);
        }
 	}
}
void lcd_draw_cursor(uint8 x, uint8 y)
{
    if ((x>=LCD_WIDTH) || (y>=LCD_HEIGHT))
    {
        return;
    }
}

