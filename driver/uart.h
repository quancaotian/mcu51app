#ifndef __UART_H__
#define __UART_H__

#include "basetype.h"
#include "platform.h"

typedef enum
{
    UART_COM0=0,
    UART_COM1,
    UART_COM2,
    UART_COM3,
    UART_COM4
}UART_ID;

bool uart_init(UART_ID uart);
bool uart_putchar(UART_ID uart, char ch);
bool uart_getchar(UART_ID uart, char* pch);
void uart_print(UART_ID uart,const char* str);
void uart_print_value(UART_ID uart,uint8 value,uint8 radix);

#endif
