#ifndef __TIME_H__
#define __TIME_H__

#include "basetype.h"

uint8 systick_gettickms(void);
void systick_init(uint8 ticks_ms);

#endif
