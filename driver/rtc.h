#ifndef __RTC_H__
#define __RTC_H__
#include "basetype.h"
typedef struct{
uint8 year;
uint8 month;
uint8 date;
uint8 hour;
uint8 min;
uint8 sec;
}RTCTIME_S;

void rtc_gettime(RTCTIME_S* rtctime);
void rtc_settime(RTCTIME_S* rtctime);
#endif