#include "digitron.h"
#include "gpio.h"
#include "trace.h"

#define MAX_DIGITAL       16
#define MAX_POSITION      6
#define DIGITRON_SEGLCK   GPIO_DU       /* 段选 */
#define DIGITRON_POSLCK   GPIO_WE       /* 位选 */

/**************************
     __      A
    |__|    FGB
    |__|.   EDCH

数字1,点亮BC段  HGFEDCBA
共阴码为0x06    00000110
**************************/
#define DIGITRON_DATA     P0            /* 数据口 */
uint8 code table_digital[MAX_DIGITAL]={
0x3f,0x06,0x5b,0x4f,
0x66,0x6d,0x7d,0x07,
0x7f,0x6f,0x77,0x7c,
0x39,0x5e,0x79,0x71};

uint8 code table_pos[MAX_POSITION]={
0xfe,0xfd,0xfb,0xf7,0xef,0xdf};

void digitron_display(uint8 pos, uint8 digital)
{         
    if (pos<MAX_POSITION && digital<MAX_DIGITAL)
    {   
        gpio_reset(DIGITRON_SEGLCK);
        DIGITRON_DATA = table_digital[digital];
        gpio_set(DIGITRON_SEGLCK);
        gpio_reset(DIGITRON_SEGLCK);
    
        gpio_reset(DIGITRON_POSLCK);
        DIGITRON_DATA = table_pos[pos];
        gpio_set(DIGITRON_POSLCK);
        gpio_reset(DIGITRON_POSLCK);
        msleep(5);
    }
}

