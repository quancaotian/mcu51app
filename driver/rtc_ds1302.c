#include "rtc.h"
#include "board.h"
#include "gpio.h"
#include "trace.h"

#define USE_BURST_MODE              1       /* 使用突发模式读写时钟 */
                                            /*|  7|  6|  5|  4|  3|  2|  1|  0| */
#define DS1302_WRITE_SEC_CMD        0x80    /*| CH|    10s    |     second    | CH(1:时钟暂停,0:时钟运行) */
#define DS1302_WRITE_MIN_CMD        0x82    /*|  0|    10m    |     minute    | */
#define DS1302_WRITE_HOUR_CMD       0x84    /*| HM|    10h    |     hour      | HM(1:12小时制,0:24小时制) */
#define DS1302_WRITE_DATE_CMD       0x86    /*| 0 | 0 |  10d  |     date      | 日期*/
#define DS1302_WRITE_MON_CMD        0x88    /*| 0 | 0 | 0 |10M|     month     | 月份*/
#define DS1302_WRITE_DAY_CMD        0x8A    /*| 0 | 0 | 0 | 0 |  0|  day      | 星期几*/
#define DS1302_WRITE_YEAR_CMD       0x8C    /*|    10year     |     year      | 年份(2000年起)*/
#define DS1302_CONTROL_WR_CMD       0x8E    /*| WP|  0|  0|  0|  0|  0|  0|  0| WP(1:写保护,0:允许写) */
#define DS1302_CLOCK_BURST_WR_CMD   0xBE    /* 突发模式写入寄存器 */

#define DS1302_READ_SEC_CMD         0x81
#define DS1302_READ_MIN_CMD         0x83
#define DS1302_READ_HOUR_CMD        0x85
#define DS1302_READ_DATE_CMD        0x87
#define DS1302_READ_MON_CMD         0x89
#define DS1302_WRITE_DAY_CMD        0x8B
#define DS1302_READ_YEAR_CMD        0x8D
#define DS1302_CONTROL_RD_CMD       0x8F    /* 读取控制寄存器 */
#define DS1302_CLOCK_BURST_RD_CMD   0xBF    /* 突发模式读取寄存器 */

#if USE_BURST_MODE
static uint8 timebuf[8]={0};
#endif

static void write_byte(uint8 by)
{
    uint8 i;
    /* 上升沿写入数据(控制字) */
    for (i=0; i<8; i++)
    {
        if ((by>>i)&0x01)
        {
            gpio_set(RTC_SDIO);
        }
        else
        {
            gpio_reset(RTC_SDIO);
        }
        gpio_reset(RTC_SCLK);
        gpio_set(RTC_SCLK);
    }
}

static uint8 read_byte()
{
    uint8 i;
    uint8 value=0;
    /* 下降沿读取数据 */
    for (i=0; i<8; i++)
    {  
        value=value>>1;/* 移位一定要放这,如果放最后则会读不到最高位 */
        gpio_set(RTC_SCLK);
        gpio_reset(RTC_SCLK);
        if (gpio_get(RTC_SDIO))
        {
            value=value|0x80;
        }
        else
        {
            value = value&0x7F;
        }
    }
    return value;
}

static void ds1302_single_write(uint8 cmd,uint8 value)
{	
    uint8 i;
	gpio_reset(RTC_RST);    /* 数据传送中止 */
	gpio_reset(RTC_SCLK);   /* 清零时钟总线 */
	gpio_set(RTC_RST);      /* 拉高RST,逻辑控制有效 */
    write_byte(cmd);
    write_byte(value);
	gpio_reset(RTC_RST);
}
static uint8 ds1302_single_read(uint8 cmd)
{
	uint8 i;
    uint8 value=0;
	gpio_reset(RTC_RST);    /* 数据传送中止 */
	gpio_reset(RTC_SCLK);   /* 清零时钟总线 */
	gpio_set(RTC_RST);      /* 拉高RST,逻辑控制有效 */
	write_byte(cmd);
    value = read_byte();
	gpio_set(RTC_SCLK);
	gpio_reset(RTC_RST);
	return(value);
}

static void ds1302_busrt_write(uint8* value)
{	
    uint8 i;
	gpio_reset(RTC_RST);    /* 数据传送中止 */
	gpio_reset(RTC_SCLK);   /* 清零时钟总线 */
	gpio_set(RTC_RST);      /* 拉高RST,逻辑控制有效 */
    write_byte(DS1302_CLOCK_BURST_WR_CMD);
    for (i=0; i<8; i++)
    {
        write_byte(value[i]);
    }
	gpio_reset(RTC_RST);
}
static uint8 ds1302_burst_read(uint8 *value)
{
	uint8 i;
	gpio_reset(RTC_RST);    /* 数据传送中止 */
	gpio_reset(RTC_SCLK);   /* 清零时钟总线 */
	gpio_set(RTC_RST);      /* 拉高RST,逻辑控制有效 */

    write_byte(DS1302_CLOCK_BURST_RD_CMD);
    for (i=0; i<8; i++)
    {
        value[i]=read_byte();
    }
	gpio_set(RTC_SCLK);
	gpio_reset(RTC_RST);
	return(value);
}

static uint8 bcd2dec(uint8 bcd)
{
    uint8 dec=bcd>>4;
    return(dec=dec*10+(bcd&=0x0F));
}

/* 获取和设置时间最好采取burst方式,一次性读写所有数据,防止出现数据错误的现象:
 * 当时间为00:06:59时,如果读取完了分钟06,刚过了1秒,则读取秒则是00,
 * 以至于读到错误数据00:06:00 */
void rtc_gettime(RTCTIME_S* rtctime)
{
#if !USE_BURST_MODE
    rtctime->sec    = bcd2dec(ds1302_single_read(DS1302_READ_SEC_CMD)&0x7F);
	rtctime->min    = bcd2dec(ds1302_single_read(DS1302_READ_MIN_CMD));
	rtctime->hour   = bcd2dec(ds1302_single_read(DS1302_READ_HOUR_CMD)&0x7F);
	rtctime->date   = bcd2dec(ds1302_single_read(DS1302_READ_DATE_CMD));
	rtctime->month  = bcd2dec(ds1302_single_read(DS1302_READ_MON_CMD));
	rtctime->year   = bcd2dec(ds1302_single_read(DS1302_READ_YEAR_CMD));
#else
    ds1302_burst_read(timebuf);
    rtctime->sec    = bcd2dec(timebuf[0]&0x7F);
	rtctime->min    = bcd2dec(timebuf[1]);
	rtctime->hour   = bcd2dec(timebuf[2]&0x7F);
	rtctime->date   = bcd2dec(timebuf[3]);
	rtctime->month  = bcd2dec(timebuf[4]);
	rtctime->year   = bcd2dec(timebuf[6]);
#endif	
}

void rtc_settime(RTCTIME_S* rtctime)
{
#if !USE_BURST_MODE
    uint8 value;   
    /* 关闭写保护 */
    ds1302_single_write(DS1302_CONTROL_WR_CMD, 0x00);

    value = rtctime->year%100;
    value = value/10*16+value%10;
   	ds1302_single_write(DS1302_WRITE_YEAR_CMD,value);
    
    value = rtctime->month%13;
    if (value!=0)
    {
        value = value/10*16+value%10;
       	ds1302_single_write(DS1302_WRITE_MON_CMD,value);
    }
    
    value = rtctime->date%32;
    if (value!=0)
    {
        value = value/10*16+value%10;
       	ds1302_single_write(DS1302_WRITE_DATE_CMD,value);
    }

    value = rtctime->hour%24;
    value = value/10*16+value%10;
    ds1302_single_write(DS1302_WRITE_HOUR_CMD,value);

    value = rtctime->min%60;
    value = value/10*16+value%10;
    ds1302_single_write(DS1302_WRITE_MIN_CMD,value);

    value = rtctime->sec%60;
    value = value/10*16+value%10;
    ds1302_single_write(DS1302_WRITE_SEC_CMD,value);

    /* 开启写保护 */
    ds1302_single_write(DS1302_CONTROL_WR_CMD, 0x80);
#else
    timebuf[0] = rtctime->sec%60;
    timebuf[0] = timebuf[0]/10*16+timebuf[0]%10;

    timebuf[1] = rtctime->min%60;
    timebuf[1] = timebuf[1]/10*16+timebuf[1]%10;

    timebuf[2] = rtctime->hour%24;
    timebuf[2] = timebuf[2]/10*16+timebuf[2]%10;
    
    timebuf[3] = rtctime->date%32;
    if (timebuf[3]!=0)
    {
        timebuf[3] = timebuf[3]/10*16+timebuf[3]%10;
    }
    
    timebuf[4] = rtctime->month%13;
    if (timebuf[4]!=0)
    {
        timebuf[4] = timebuf[4]/10*16+timebuf[4]%10;
    }

    /* 星期需要根据日期进行计算好后再设置(Zeller蔡勒公式),这里不实现 */
    timebuf[5] = 0;
   
	timebuf[6] = rtctime->year%100;
    timebuf[6] = timebuf[6]/10*16+timebuf[6]%10;

    timebuf[7] = 0x00;

    ds1302_single_write(DS1302_CONTROL_WR_CMD, 0x00);
	ds1302_busrt_write(timebuf);
	ds1302_single_write(DS1302_CONTROL_WR_CMD, 0x80);
#endif
}

