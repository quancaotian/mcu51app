#include "platform.h"
#include "uart.h"
#include "basetype.h"
#include "trace.h"

#define     RXBUF_SIZE          8   /* 设置接收缓存大小为8字节 */
typedef struct{
    sint8 m_rx_iput;
    sint8 m_rx_iget;
    uint8 m_rx_max;
    uint8 m_rx_count;
}uart_s;

static uart_s s_uart[UART_COUNT];
static uint8 s_rxbuf[RXBUF_SIZE];

/* 中断服务程序 */
void isr_uart0_handler(void) interrupt 4 using 2
{
    if (RI)
    {
        RI = 0;
        if(s_uart[0].m_rx_count != s_uart[0].m_rx_max)
        {
            s_uart[0].m_rx_iput = (s_uart[0].m_rx_iput+1) % s_uart[0].m_rx_max;
            s_rxbuf[s_uart[0].m_rx_iput]=SBUF;
            s_uart[0].m_rx_count++;
        }
    }
}

bool uart_init(UART_ID id)
{
    if (id>=UART_COUNT)
    {
        return false;
    }
	s_uart[id].m_rx_iget = -1;
	s_uart[id].m_rx_iput = -1;
	s_uart[id].m_rx_max = RXBUF_SIZE;
	s_uart[id].m_rx_count = 0;
    
    SCON = 0x50;    /* 模式1,8bit数据位,使能接收 */
    TMOD |= 0x20;   /* timer1,mode 2,8-bit reload */
    TH1 = 0xFD;     /* 11.0592M时钟下,9600波特率 */
    TL1 = 0xFD;     /* 重装值=TH1 */
    TR1 = 1;        /* timer1 run */
    EA = 1;         /* 开中断 */
    ES = 1;         /* 使能串口中断 */
    TI = 0;         /* 清除发送中断 */
    return true;
}

bool uart_getchar(UART_ID id,uint8* ch)
{
    if (id>=UART_COUNT)
    {
        return false;
    }
    if(0==s_uart[id].m_rx_count)
    {
        return false;
    }
    s_uart[id].m_rx_iget = (s_uart[id].m_rx_iget+1) % s_uart[id].m_rx_max;
    *ch = s_rxbuf[s_uart[id].m_rx_iget];
    s_uart[id].m_rx_count--;
    return true;
}

bool uart_putchar(UART_ID id, char ch)
{
    if (id>=UART_COUNT)
    {
        return false;
    }
    SBUF = ch;
    while(TI!=1);/* 等待发送完毕 */
    TI=0;
    return true;
}

void uart_print(UART_ID uart_id,const char* str)
{
    char* pchr=(char*)str;
    while(*pchr)
    {
        uart_putchar(uart_id, *pchr);
        pchr++;
    }
}

void uart_print_value(UART_ID uart_id,uint8 value,uint8 radix)
{
    char chr;
    if (radix==16)
    {
        uart_putchar(uart_id, '0');
        uart_putchar(uart_id, 'x');
        chr=DIGIT2ASCII((value&0xF0)>>4);
        uart_putchar(uart_id, chr);
    
        chr=DIGIT2ASCII(value&0x0F);
        uart_putchar(uart_id, chr);
    }
    else if (radix==10)
    {
        chr=DIGIT2ASCII(value/100);
        if(chr!='0')
        {
            uart_putchar(uart_id, chr);
            chr=DIGIT2ASCII(value/10);
            uart_putchar(uart_id, chr);
       }
       else
       {
             chr=DIGIT2ASCII(value/10);
             if(chr!='0')
             {
                uart_putchar(uart_id, chr);
             }
       }
       chr=DIGIT2ASCII(value%10);
       uart_putchar(uart_id, chr);
       
    }
}


