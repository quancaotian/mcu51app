#include "board.h"
#include "gpio.h"
#include "trace.h"
#include "i2c.h"

#define TIME_KP    5  /* I2C保持时间 */

/* I2C开始信号:SCL高电平期间,SDA由高变低 */
static void start(void)
{
    gpio_set(I2C_SDA);
    usleep(TIME_KP);
    gpio_set(I2C_SCL);
    usleep(TIME_KP);
    gpio_reset(I2C_SDA);
    usleep(TIME_KP);
}

/* I2C停止信号:SCL高电平期间,SDA由低变高 */
static void stop(void)
{
    gpio_reset(I2C_SDA);
    usleep(TIME_KP);
    gpio_set(I2C_SCL);
    usleep(TIME_KP);
    gpio_set(I2C_SDA);
    usleep(TIME_KP);
}

/* I2C主机应答信号:SDA为低电平,表示主机接收字节完毕 */
static void ack(void)
{
    gpio_reset(I2C_SDA);
	usleep(TIME_KP);
	gpio_set(I2C_SCL);
	usleep(TIME_KP);
	gpio_reset(I2C_SCL);
	usleep(TIME_KP);
}

/* I2C主机非应答信号:SDA为高电平,表示主机结束接收从机数据 */
static void noack(void)
{
    gpio_set(I2C_SDA);
	usleep(TIME_KP);
	gpio_set(I2C_SCL);
	usleep(TIME_KP);
	gpio_reset(I2C_SCL);
	usleep(TIME_KP);
}

/* I2C主机等待ACK信号:等待SDA线被拉低 */
static void wait_ack(void)
{
    uint8 timeout=250;
    gpio_set(I2C_SCL);
    usleep(TIME_KP);
	while(timeout--)
	{
        if(gpio_get(I2C_SDA)==0)
        {
            gpio_reset(I2C_SCL);
            usleep(TIME_KP);
            return;
        }
	}
	gpio_reset(I2C_SCL);
    usleep(TIME_KP);
	TRACE_ERR("i2c write timeout.\r\n");
}

static void write_byte(uint8 by)
{
    uint8 i;
    for(i=0; i<8; i++)
    {
        gpio_reset(I2C_SCL);/* 写前拉低SCL,为产生时序做准备 */
        usleep(TIME_KP);
        if ((by<<i)&0x80)
        {
            gpio_set(I2C_SDA);
        }
        else
        {
            gpio_reset(I2C_SDA);
        }
        usleep(TIME_KP);
        gpio_set(I2C_SCL);
        usleep(TIME_KP);
    }
    gpio_reset(I2C_SCL);/* 写完后拉低SCL */
	usleep(TIME_KP);
	gpio_set(I2C_SDA);  /* 写完后拉高SDA,因为后续要等待ACK信号 */
	usleep(TIME_KP);
}

static uint8 read_byte(void)
{
    uint8 i;
    uint8 ret=0;
    gpio_reset(I2C_SCL);/* 读前拉低SCL,为产生时序做准备 */
    usleep(TIME_KP);
    gpio_set(I2C_SDA);
    usleep(TIME_KP);
    for(i=0;i<8;i++)
    {
        gpio_set(I2C_SCL);
        usleep(TIME_KP);
        ret = (ret<<1) | gpio_get(I2C_SDA);
        gpio_reset(I2C_SCL);/* 读完后拉低SCL */
        usleep(TIME_KP);    
    }
    return ret;
}


void i2c_select(uint8 i2cbus)
{  
}

void i2c_init(uint8 i2cbus)
{
    /* RTC ds1302与I2C共用了两根线,所以使用I2C必须将其禁止掉 */
    gpio_reset(RTC_RST);
    gpio_reset(I2C_SDA);
    usleep(TIME_KP);
    gpio_reset(I2C_SCL);
    usleep(TIME_KP);
}

sint32 i2c_reg_read(uint8 i2cdev,uint8 reg, uint8* value)
{
    /* 先写入要读取的寄存器地址 */
    start();
    write_byte(i2cdev&0xFE);
    wait_ack();
    write_byte(reg);
    wait_ack();

    /* 改变传送方向 */
    start();
    write_byte(i2cdev|0x01);
    wait_ack();

    /* 读取数据 */
    *value=read_byte();
    noack();
    stop();
    return 0;
}

sint32 i2c_reg_write(uint8 i2cdev,uint8 reg, uint8 value)
{
    start();
    write_byte(i2cdev&0xFE);
    wait_ack();
    write_byte(reg);
    wait_ack();
    write_byte(value);
    wait_ack();
    stop();
    return 0;
}

sint32 i2c_read(uint8 i2cdev, uint8 * wbuf, sint32 wlen, uint8 * rbuf, sint32 rlen)
{
    return 0;
}
sint32  i2c_write(uint8 i2cdev, uint8 * data, int len)
{
    return 0;
}
