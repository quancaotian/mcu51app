#ifndef __BOARD_H__
#define __BOARD_H__

/* 定义使用的板子 */
#include "hjboard.h"

/* 每个xxboard.c都应该实现下列函数 */
void msleep(uint16 ms);
void usleep(uint8 us);
void alarm(bool enable);
void board_init();
#endif