#include "board.h"
#include "basetype.h"
#include "gpio.h"

void msleep(uint16 ms)
{
    uint8 k;
    while(ms--)
    {
        for(k = 0; k < 120; k++);
    }
}

void usleep(uint8 us)
{
	while(us--);
}

void alarm(bool enable)
{
    if (enable)
    {
        GPIO_FM = 0;
    }
    else
    {
        GPIO_FM = 1;
    }
}

void board_init()
{
    gpio_reset(GPIO_DU);
    gpio_reset(GPIO_WE);
}