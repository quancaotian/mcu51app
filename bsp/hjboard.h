#ifndef __HJBOARD_H__
#define __HJBOARD_H__
/**********************************************************
 File   : hjstm32.h
 Bref   : ?۾?HL_1C??????ͷ?ļ? 
 Version: V1.0.0
 Author : FergusZeng
 Date   : 2013-12-08
***********************************************************/
#include "platform.h"

/* ????????IO?? */
#define KEY_K1      (P3_4)
#define KEY_K2      (P3_5)
#define KEY_K3      (P3_6)
#define KEY_K4      (P3_7)

/*---------------------------------------------
               4X4????????
  P3.4 P3.5 P3.6 P3.7
  |    |    |    |
  S1---S2---S3---S4----P3.0
  |    |    |    |
  S5---S6---S7---S8----P3.1
  |    |    |    |
  S9---S10--S11--S12---P3.2
  |    |    |    |
  S13--S14--S15--S16---P3.3
  --------------------------------------------*/
#define KSCAN_X0        (P3_4)      /* ˮƽ0 */
#define KSCAN_X1        (P3_5)      /* ˮƽ1 */
#define KSCAN_X2        (P3_6)      /* ˮƽ2 */
#define KSCAN_X3        (P3_7)      /* ˮƽ3 */
#define KSCAN_Y0        (P3_0)      /* ??ֱ0 */
#define KSCAN_Y1        (P3_1)      /* ??ֱ1 */
#define KSCAN_Y2        (P3_2)      /* ??ֱ2 */
#define KSCAN_Y3        (P3_3)      /* ??ֱ3 */

/*  LED??ˮ??IO??  */
#define LED_L0          (P1_0)    
#define LED_L1          (P1_1)
#define LED_L2          (P1_2)
#define LED_L3          (P1_3)
#define LED_L4          (P1_4)
#define LED_L5          (P1_5)
#define LED_L6          (P1_6)
#define LED_L7          (P1_7)

/* I2C */
#define I2C_SDA         (P2_0)
#define I2C_SCL         (P2_1)

/* DS1302 RTC */
#define RTC_SDIO        (P2_0)
#define RTC_SCLK        (P2_1)
#define RTC_RST         (P2_4)

/* ????GPIO */
#define GPIO_D0         (P0_0)
#define GPIO_D1         (P0_1)
#define GPIO_D2         (P0_2)
#define GPIO_D3         (P0_3)
#define GPIO_D4         (P0_4)
#define GPIO_D5         (P0_5)
#define GPIO_D6         (P0_6)
#define GPIO_D7         (P0_7)

#define GPIO_DB1        (P1_0)
#define GPIO_DB2        (P1_1)
#define GPIO_DB3        (P1_2)
#define GPIO_DB4        (P1_3)
#define GPIO_DB5        (P1_4)
#define GPIO_DB6        (P1_5)
#define GPIO_DB7        (P1_6)
#define GPIO_DB8        (P1_7)

#define GPIO_18B20      (P2_2)  /* ?¶ȸ?Ӧ?? */
#define GPIO_FM         (P2_3)
#define GPIO_DU         (P2_6)  /* ?????ܶ?ѡ?ź? */
#define GPIO_WE         (P2_7)  /* ??????λѡ?ź? */
#define GPIO_LCDEN      (P2_5)

#define GPIO_INT1       (P3_3)


#endif
