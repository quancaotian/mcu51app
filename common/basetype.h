#ifndef __BASETYPE_H__
#define __BASETYPE_H__

#include "config.h"

/* Keil C51中enum类型为1字节 */
typedef unsigned char   uint8;
typedef signed char     sint8;
typedef unsigned int    uint16;
typedef signed int      sint16;
typedef unsigned int    uint32;
typedef signed int      sint32;

/* 定义bool类型 */
typedef char bool;
enum
{
	false=0,
	true=!false
};
enum
{
    API_OK          =  0,
    API_ERR         = -1,
    API_TIMEOUT     = -2,
    API_REBOOT      = -3,
    API_RESET       = -4,
    API_FULL        = -5,
    API_NULL        = -6,
};

/* 定义常用宏 */
#define DIGIT2ASCII(x)  (((x)>=0x0A)?((x)-0x0A+'A'):((x)+'0'))
#define ASCII2DIGIT(x)  (((x)>='a')?((x)-'a'+0x0A):(((x)>='A')?((x)-'A'+0x0A):((x)-'0')))

#define MAX(a,b)            (((a)>(b))?(a):(b))
#define MIN(a,b)            (((a)<(b))?(a):(b))
#define CLIP(min,x,max)     (((x)>(max))?(max):(((x)<(min))?(min):(x)))
#endif

