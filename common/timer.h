/******************************************************************************
* @file     :   timer.h
* @bref     :   software timer header file
* @version  :   v.1.0
* @date     :   2013-12-12
* @author   :   FergusZeng
* @note     :   软件定时器
                最多可创建128个定时器(可根据mcu资源进行配置)
                最小精度为100ms
                最长定时为3276800ms
******************************************************************************/
#ifndef __TIMER_H__
#define __TIMER_H__

#include "basetype.h"


/* 当condition为true,则忙等,直至timeout */
#define WAIT_ON(condition,timeout)  {sint32 i = timeout;\
                                   while(condition){i--;if(i<=0){goto TIMEOUT;}}}

typedef enum{
    TIMERTYPE_ONESHOT=0,
    TIMERTYPE_PERIODIC,
}TIMERTYPE_E;

typedef sint8 tid_t;
typedef void (*CALLBACK_ONTIMER)(void);

tid_t timer_alloc(TIMERTYPE_E timertype,uint16 ms,CALLBACK_ONTIMER callback_ontimer);
bool timer_start(tid_t timerid);
bool timer_stop(tid_t timerid);
#endif
