/******************************************************************************
* @file     :   timer.c
* @bref     :   software timer implementation
* @version  :   v.1.0
* @date     :   2013-12-12
* @author   :   FergusZeng
* @note     :   软件定时器
                最多可创建128个定时器(可根据mcu资源进行配置)
                最小精度为100ms
                最长定时为3276800ms
******************************************************************************/
#include <stdio.h>
#include "platform.h"
#include "timer.h"
#include "systick.h"
#include "config.h"

extern uint8 SYSTEM_TICKS_MS;/* 系统定时器的时钟片周期 */
/*-------------------------------------------------------------------------
* 如果系统定时器的时钟片周期我们设置为100ms的定时精度,
* 则最长定时时间则为2^16*100ms=6553600ms=6553s
-------------------------------------------------------------------------*/
typedef struct{
    uint8 m_isused:1;   /* 0:未使用,1:已使用 */
    uint8 m_isstart:1;  /* 0:停止,1:开始 */
    uint8 m_timertype:4;/* 定时器类型 */
    uint16 m_timeout;   /* 定时时间片数 */
    CALLBACK_ONTIMER m_callback_ontimer;
}timer_s;
static timer_s s_timerpool[TIMER_COUNT]={0};
static uint16 s_mytimerticks=0;

tid_t timer_alloc(TIMERTYPE_E timertype,uint16 ms,CALLBACK_ONTIMER callback_ontimer )
{
    uint8 i;
    for (i=0;i<TIMER_COUNT; i++)
    {
        if (s_timerpool[i].m_isused==0)
        {
            s_timerpool[i].m_isused=true;
            s_timerpool[i].m_isstart=false;
            s_timerpool[i].m_timertype = timertype;
            s_timerpool[i].m_timeout = ms/(SYSTEM_TICKS_MS);
            s_timerpool[i].m_callback_ontimer = callback_ontimer;
            return i;
        }
    }
    return -1;
}

bool timer_start(tid_t timerid)
{
    if (timerid>=0 &&
        timerid<TIMER_COUNT&&
        (s_timerpool[timerid].m_isused==1) &&
        (s_timerpool[timerid].m_callback_ontimer!=0))
    {
        s_timerpool[timerid].m_isstart=true;
        return true;
    }
    return false;
}

bool timer_stop(tid_t timerid)
{
    if (timerid>=0 &&
        timerid<TIMER_COUNT &&
        (s_timerpool[timerid].m_isused==1))
    {
        s_timerpool[timerid].m_isstart=false;
        return true;
    }
    return false;
}

/* 该函数用于给系统定时器回调,回调频率为硬件定时器中断频率,以此作为事件标准 */
void isr_systick_doticker(void)
{
    uint8 i=0;
    s_mytimerticks++;
    /* 每隔SYSTEM_TICKS_MS执行一次 */
    for(i=0;i<TIMER_COUNT;i++)
    {
        if(s_timerpool[i].m_isstart)
        {
            if(s_mytimerticks%(s_timerpool[i].m_timeout)==0)
            {
                if (s_timerpool[i].m_callback_ontimer!=NULL)
                {
                   s_timerpool[i].m_callback_ontimer(); 
                }
                if(TIMERTYPE_ONESHOT==s_timerpool[i].m_timertype)
                {
                    s_timerpool[i].m_isused=false;
                }
            }
        }
    }    
}

