#ifndef __TRACE_H__
#define __TRACE_H__

#include "platform.h"

/* 定义是否打印debug信息 */
#define DEBUG_VERSION       1 


/* 定义是否使用标准库提供的printf函数 */
#define USE_PRINTF_FUNCTION      0

/********************************************************************************/
#if USE_PRINTF_FUNCTION
    #include <stdio.h>
    #if DEBUG_VERSION
    #define TRACE_DBG(fmt,arg...)   {printf("\033[33m\033[1m"fmt"\033[0m\r\n",##arg);}
    #else
    #define TRACE_DBG(fmt,arg...)
    #endif
	#define TRACE_REL(fmt,arg...)   {printf("\033[32m\033[1m"fmt"\033[0m\r\n",##arg);}
    #define TRACE_ERR(fmt,arg...)   {printf("\033[31m\033[1m"fmt"\033[0m\r\n",##arg);}			  
    #define TRACE_HEX(bstr,an_uint8,estr)
    #define TRACE_DEC(bstr,an_uint8,estr)
    #define TRACE_CHR(bstr,ch,estr)
    
#else                   /* 不使用printf函数 */
    #include "uart.h"
    #define UART_PRINT_DEV   UART_COM0
    
    #if DEBUG_VERSION
    #define TRACE_DBG(str)   uart_print(UART_PRINT_DEV,str);
    #else
    #define TRACE_DBG(str)
    #endif
    
    #define TRACE_REL(str)   uart_print(UART_PRINT_DEV,str);
    #define TRACE_ERR(str)   uart_print(UART_PRINT_DEV,str);
    #define TRACE_HEX(bstr,an_uint8,estr)   {uart_print(UART_PRINT_DEV,bstr);\
                                          uart_print_value(UART_PRINT_DEV,an_uint8,16);\
                                          uart_print(UART_PRINT_DEV,estr);}
    #define TRACE_DEC(bstr,an_uint8,estr)   {uart_print(UART_PRINT_DEV,bstr);\
                                          uart_print_value(UART_PRINT_DEV,an_uint8,10);\
                                          uart_print(UART_PRINT_DEV,estr);}
    #define TRACE_CHR(bstr,ch,estr)      {uart_print(UART_PRINT_DEV,bstr);\
                                          uart_putchar(UART_PRINT_DEV,ch);\
                                          uart_print(UART_PRINT_DEV,estr);}
#endif

/********************************************************************************/
#if DEBUG_VERSION
    #define my_assert_param(expr) ((expr) ? (void)0 : my_assert_failed(__FILE__, __LINE__))
    void my_assert_failed(char* file, uint32 line);
	#define ASSERT(cond)    my_assert_param(cond)
#else
	#define ASSERT(cond)
#endif
/********************************************************************************/

#endif
