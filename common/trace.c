#include "trace.h"

#if DEBUG_VERSION
#if USE_PRINTF_FUNCTION
void my_assert_failed(char * file, uint32 line)
{
    TRACE_ERR("ASSERT FAILED:%s,L%d\n",file,line);
    while(1){};
}
#else
void my_assert_failed(char * file, uint32 line)
{
    TRACE_ERR("ASSERT FAILED:");
    TRACE_ERR(file);
    while(1){};
}
#endif
#endif